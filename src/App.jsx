import './App.css'
import ICons from './components/ICons'
import Toast from './components/Toast'
import Modals from './components/Modals'
import ToolTip from './components/ToolTip'
import CountUps from './components/CountUps'
import IdleTimerContainer from './components/IdleTimerContainer'
import ColorPickers from './components/ColorPickers'
import CreditCards from './components/CreditCards'
import DatePickers from './components/DatePickers'
import VideoPlayers from './components/VideoPlayers'
import LoadingIndicators from './components/LoadingIndicators'
import LineCharts from './components/Charts/LineCharts.jsx'

function App () {
  return (
    <div>
      {/* <ICons /> */}
      {/* <Toast /> */}
      {/* <Modals /> */}
      {/* <ToolTip /> */}
      {/* <CountUps /> */}
      {/* <IdleTimerContainer /> */}
      {/* <ColorPickers /> */}
      {/* <CreditCards /> */}
      {/* <DatePickers /> */}
      {/* <VideoPlayers /> */}
      {/* <LoadingIndicators /> */}
      <div style={{ width: 700 }}>
        <LineCharts />
      </div>
    </div>
  )
}

export default App
