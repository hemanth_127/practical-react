import React, { useState } from 'react'
import { UserData } from './Data'
import { Line, Pie } from 'react-chartjs-2'
import { Chart as ChartJS } from 'chart.js/auto'
import BarChart from './BarChart'

function LineCharts () {
  const [userData, setUserData] = useState({
    labels: UserData.map(data => data.year),

    datasets: [
      {
        label: 'Users Gained',
        data: UserData.map(data => data.userGain),
        backgroundColor: [
          'rgba(75,192,192,1)',
          '#ecf0f1',
          '#50AF95',
          '#f3ba2f',
          '#2a71d0'
        ],
        borderColor: 'black',
        borderWidth: 2
      }
    ]
  })

  return (
    <div>
      <h2>Line Chart</h2>
      <Line data={userData} />
      <h2>Bar Chart</h2>
      <BarChart data={userData} />
      <h2>Pie Chart</h2>
      <Pie data={userData} />
    </div>
  )
}

export default LineCharts
