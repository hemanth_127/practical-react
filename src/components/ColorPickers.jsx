import React, { useState } from 'react'
import { ChromePicker } from 'react-color'

function ColorPickers () {
  const [color, setColor] = useState('#fff')
  const [showColorPicker, setShowColorPicker] = useState(false)

  return (
    <div>
      <h2>you picked {color}</h2>
      <button
        onClick={() => setShowColorPicker(showColorPicker => !showColorPicker)}
      >
        {showColorPicker ? 'close' : 'pick a color'}
      </button>
      {showColorPicker && (
        <ChromePicker
          color={color}
          onChange={updatedColor => setColor(updatedColor.hex)}
        />
      )}
    </div>
  )
}

export default ColorPickers
