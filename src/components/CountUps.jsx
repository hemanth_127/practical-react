import React from 'react'
import CountUp, { useCountUp } from 'react-countup'

function CountUps () {
  const countRef = React.useRef(null)
  const { start, pauseResume, reset, update } = useCountUp({
    ref: countRef,
    duration: 5,
    start: 0,
    end: 50000
  })

  return (
    <>
      <div>
        <h1 ref={countRef} />
        <button onClick={start}>Start</button>
        <button onClick={pauseResume}>Pause/Resume</button>
        <button onClick={reset}>reset</button>
        <button onClick={() => update(2000)}> update</button>
      </div>

      <div>
        <CountUp end={100} />
      </div>

      <div>
        <CountUp start={200} end={500} duration={5} />
      </div>

      <div>
        <CountUp end={1000} duration={5} prefix='$' decimals={2} />
      </div>

      <div>
        <CountUp end={1000} duration={5} decimals={2} suffix='USD' />
      </div>
    </>
  )
}

export default CountUps
