import React from 'react'
import { FaReact } from 'react-icons/fa'
import { MdAccessAlarms } from 'react-icons/md'
import { IconContext } from 'react-icons'

function ICons () {
  return (
    <div>
      <IconContext.Provider value={{ color: 'blue', size: '5rem' }}>
        <div>
          <FaReact />
          <MdAccessAlarms />
        </div>
      </IconContext.Provider>
    </div>
  )
}

export default ICons
