import React, { useRef, useState } from 'react'
import { useIdleTimer } from 'react-idle-timer'
import Modal from 'react-modal'

Modal.setAppElement('#root')

function IdleTimerContainer () {
  const idleTimerRef = useRef(null)
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [isloggedIn, setIsloggedIn] = useState(true)
  const sessesionTimeoutRef = useRef(null)

  const onIdle = () => {
    if (isloggedIn) {
      console.log('User is Idle')
      setIsModalOpen(true)
      sessesionTimeoutRef.current = setTimeout(logOut, 5000)
    }
  }

  const idleTimer = useIdleTimer({
    ref: idleTimerRef,
    onIdle: onIdle,
    timeout: 5 * 1000
  })

  const stayActice = () => {
    setIsModalOpen(false)
    clearTimeout(sessesionTimeoutRef.current)
  }

  const logOut = () => {
    setIsModalOpen(false)
    setIsloggedIn(false)
    clearTimeout(sessesionTimeoutRef.current)
  }

  return (
    <div>
      {isloggedIn ? <h2>Hello Hemanth</h2> : <h2>Hello Guest</h2>}
      <Modal isOpen={isModalOpen}>
        <h2> you've been idle for a while</h2>
        <p>you will be logged out soon</p>
        <div>
          <button onClick={logOut}>log me out</button>
          <button onClick={stayActice}>keep me signed in</button>
        </div>
      </Modal>

      <div idletimer={idleTimer}>hello</div>
    </div>
  )
}

export default IdleTimerContainer
