import React from 'react'
import { css, cx } from '@emotion/css'
import { BounceLoader, BarLoader, BeatLoader } from 'react-spinners'

const loaderCSS = css`
  margin-top: 25px;
  margin-bottom: 25px;
`

function LoadingIndicators () {
  return (
    <div>
      <BeatLoader
        className={cx(loaderCSS)}
        // loading={false}
        size={24}
        color='red'
      />
      <BounceLoader className={cx(loaderCSS)} loading size={60} color='green' />
      <BarLoader className={cx(loaderCSS)} loading size={48} color='orange' />
    </div>
  )
}

export default LoadingIndicators
