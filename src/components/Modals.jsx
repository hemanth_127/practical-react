import React, { useState } from 'react'
import ReactModal from 'react-modal'

ReactModal.setAppElement('#root')

function Modals () {
  const [open, setOpen] = useState(false)
  return (
    <div>
      <button
        onClick={() => {
          setOpen(true)
        }}
      >
        Open
      </button>

      <ReactModal
        isOpen={open}
        shouldCloseOnEsc={false}
        onRequestClose={() => setOpen(false)}
        // className='custom-modal'
        // overlayClassName='custom-overlay'
        style={{
          overlay: {
            backgroundColor: 'gray'
          },
          content: {
            color: 'blue'
          }
        }}
      >
        <h2>Welcome to Modal</h2>
        <button
          onClick={() => {
            setOpen(false)
          }}
        >
          Close
        </button>
      </ReactModal>
    </div>
  )
}

export default Modals
