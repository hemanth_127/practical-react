import React from 'react'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { Bounce } from 'react-toastify'

const CustomToast = ({ closeToast }) => {
  return (
    <div>
      Something Went Wrong
      <button onClick={closeToast}>close</button>
    </div>
  )
}

function Toast () {
  const handleNotify = () => {
    toast.info(<CustomToast />, {
      position: 'top-left',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: 'light',
      transition: Bounce
    })
    // toast('basic notification', { position: 'top-right' })
    // toast('basic notification', { position: 'top-center' })
    // toast('basic notification', { position: 'bottom-left' })
    // toast('basic notification', { position: 'bottom-right' })
    // toast('basic notification', { position: 'bottom-center' })
  }

  return (
    <div>
      {/* toast Notifications */}

      <button onClick={handleNotify}>toast</button>
      <ToastContainer />
    </div>
  )
}

export default Toast
