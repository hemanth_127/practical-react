import React, { forwardRef } from 'react'
import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'

const ColoredToolTip = () => {
  return <span style={{ color: 'green' }}>Color components</span>
}

const CustomToolTip = forwardRef((props, ref) => {
  return (
    <div ref={ref}>
      <p>first line</p>
      <div>secong line</div>
    </div>
  )
})

function ToolTip () {
  return (
    <>
      <div style={{ paddingBottom: '5rem' }}>
        <Tippy
          arrow={false}
          delay={1000}
          placement='right'
          content='Basic tooltip'
        >
          <button>Welcome</button>
        </Tippy>
      </div>

      <div style={{ paddingBottom: '3em' }}>
        <Tippy
          content={
            <span style={{ color: 'red' }}>
              <h1>Luffy</h1>
              <p>The King of Pirates</p>
            </span>
          }
        >
          <button>Luffy</button>
        </Tippy>
      </div>

      <div style={{ paddingBottom: '3em' }}>
        <Tippy content={<ColoredToolTip />}>
          <button>Zoro</button>
        </Tippy>
      </div>

      <div style={{ paddingBottom: '3em' }}>
        <Tippy content={<ColoredToolTip />} placement='top-start'>
          <CustomToolTip />
        </Tippy>
      </div>
    </>
  )
}

export default ToolTip
